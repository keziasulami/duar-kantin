from django.test import TestCase, Client

# Create your tests here.


class HomepageTest(TestCase):

    def test_pages_contains_text_tagline(self):
        response = Client().get('')
        self.assertContains(response, 'Pesan, Bayar, dan Ambil!')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_order_button(self):
        response = Client().get('')
        self.assertContains(
            response, 'Order')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_praktis_word(self):
        response = Client().get('')
        self.assertContains(response, 'Praktis')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_praktis_desc(self):
        response = Client().get('')
        self.assertContains(response, 'Beli makanan yang kamu')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_cepat_word(self):
        response = Client().get('')
        self.assertContains(response, 'Cepat')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_cepat_desc(self):
        response = Client().get('')
        self.assertContains(response, 'Sedang sibuk tapi ')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_wallet_word(self):
        response = Client().get('')
        self.assertContains(response, 'e-Wallet')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_wallet_desc(self):
        response = Client().get('')
        self.assertContains(response, 'Kantin Fasilkom bisa pake e-wal')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_why_word(self):
        response = Client().get('')
        self.assertContains(response, 'Mengapa pesan menggunak')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_bolded_green_kane(self):
        response = Client().get('')
        self.assertContains(
            response, '<span class="green-bold">kane.</span>')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_tagline_desc(self):
        response = Client().get('')
        self.assertContains(
            response, "<p class='mb-5 mt-3' id='small-desc'>Tak perlu antri, tinggal")
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_clock_images(self):
        response = Client().get('')
        self.assertContains(response, "clock.svg")
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_like_images(self):
        response = Client().get('')
        self.assertContains(response, "like.svg")
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_wallet_images(self):
        response = Client().get('')
        self.assertContains(response, "wallet.svg")
        self.assertEqual(response.status_code, 200)

    def test_right_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'home.html')
        self.assertEqual(response.status_code, 200)

    def test_contains_vertical_line(self):
        response = Client().get('')
        self.assertContains(response, '<div class="vr-line')
        self.assertEqual(response.status_code, 200)

    def test_contains_navbar(self):
        response = Client().get('')
        self.assertContains(response, '<nav')
        self.assertEqual(response.status_code, 200)

    def test_contains_logo(self):
        response = Client().get('')
        self.assertContains(response, 'logo.png')
        self.assertEqual(response.status_code, 200)

    def test_contains_bootstrap(self):
        response = Client().get('')
        self.assertContains(response, '''<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">''')
        self.assertEqual(response.status_code, 200)

    def test_contains_custom_css(self):
        response = Client().get('')
        self.assertContains(response, '''<link rel="stylesheet" href="/static/main.css">''')
        self.assertEqual(response.status_code, 200)

    def test_contains_footer(self):
        response = Client().get('')
        self.assertContains(response, '<footer')
        self.assertEqual(response.status_code, 200)

    def test_pages_loaded_google_font_PTSans(self):
        response = Client().get('')
        self.assertContains(response, '<link href="https://fonts.googleapis.com/css?family=PT+Sans:700,700i&display=swap" rel="stylesheet">')
        self.assertEqual(response.status_code, 200)

    def test_pages_loaded_google_font_Poppins(self):
        response = Client().get('')
        self.assertContains(response, '<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,700&display=swap" rel="stylesheet">')
        self.assertEqual(response.status_code, 200)

    def test_pages_load_jquery(self):
        response = Client().get('')
        self.assertContains(response, ''' <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>''')
        self.assertEqual(response.status_code, 200)

    def test_pages_load_popper_js(self):
        response = Client().get('')
        self.assertContains(response, '''<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>''')
        self.assertEqual(response.status_code, 200)

    def test_pages_head_is_kane(self):
        response = Client().get('')
        self.assertContains(response, '<title>kane.</title>')
        self.assertEqual(response.status_code, 200)

    def test_pages_contains_footer_text(self):
        response = Client().get('')
        self.assertContains(response, '©2019 kane (KAntin oNlinE), All rights reserved.')
        self.assertEqual(response.status_code, 200)


    
