from django.shortcuts import render, redirect
from django.urls import reverse

# Create your views here.
def order(request):
    context = {
        'page' : 'order',
        'title': 'Pemesanan',
        'subtitle1': 'Yuk cek pesanan kamu di sini! Terima',
        'subtitle2': 'kasih sudah menggunakan layanan kami!'
    }
    return render(request, 'order.html', context)

def make_order(request):
    pass