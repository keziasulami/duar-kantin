from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('bantuan/', include('bantuan.urls')),
    path('menu/', include('menu.urls')),
    path('order/', include('order.urls')),
    path('riwayat/', include('riwayat.urls')),
]
