# Kelompok 1 PPW kelas E
[![pipeline status](https://gitlab.com/mrifqyz/duar-kantin/badges/master/pipeline.svg)](https://gitlab.com/mrifqyz/duar-kantin/commits/master)

## Anggota:
1. Kezia Sulami
2. Mohamad Rifqy Zulkarnaen
3. Annisya Rizkia Ifani
4. Faza Ghani Irham

## Repository
https://gitlab.com/mrifqyz/duar-kantin

## Link Heroku:
https://duarkantin.herokuapp.com

## Link Wireframe:
https://wireframe.cc/LjU5NR - home  
https://wireframe.cc/uSvxo2 - menu/kantin  
https://wireframe.cc/vs9Xua - form  
https://wireframe.cc/qHF1bH - detail  
https://wireframe.cc/EM0suI - pemesanan  


## Link Figma:
https://www.figma.com/file/BTitVNJHt2AiqACZg7buIs/duarkantin

# Tentang Proyek:
KAntin oNlinE atau yang disingkat `kane.` adalah sebuah inovasi kami di bidang website yang menerapkan tema industri 4.0 dimana website ini mempermudah kita untuk memesan suatu makanan tanpa harus mengantri terlebih dahulu. Inovasi ini terinspirasi dari observasi kami sehari-hari dimana setiap jam makan siang di Fasilkom, kantin selalu ramai dengan pembeli bahkan sampai menghalangi jalan. Selain itu, inovasi ini juga terinspirasi dari cetakin.id yang menurut kami sama-sama menerapkan industri 4.0 yang mempermudah kita untuk mencetak dokumen secara _remote_. Proyek ini memungkinkan pembeli untuk:

1. Membeli makanan tanpa menunggu dan tinggal langsung ambil
2. Membayar makanan menggunakan _e-Wallet_ milik setiap counter
3. Melihat daftar kantin yang ada
4. Melihat daftar menu yang ada di setiap kantin
4. Melihat pesanan dan estimasi harga pesanan
5. Mengetahui riwayat pembelian _user_

# Daftar fitur yang akan diimplementasi:
1. Home 
2. Order (menampilkan barang yang dibeli)
3. Kantin (menampilkan daftar kantin)
4. Menu (menampilkan daftar menu kantin)
5. Riwayat (menampilkan riwayat transaksi)
6. Bantuan
