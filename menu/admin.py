from django.contrib import admin
from .models import Canteen, Food

# Register your models here.
class CanteenAdmin(admin.ModelAdmin):
	class Meta:
		model = Canteen

class FoodAdmin(admin.ModelAdmin):
	class Meta:
		model = Food

admin.site.register(Canteen, CanteenAdmin)
admin.site.register(Food, FoodAdmin)