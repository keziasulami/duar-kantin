from django.test import TestCase, Client
from .models import Canteen, Food

# Create your tests here.
class CanteenTest(TestCase):

	def test_open_canteen_page(self):
		c = Client()
		response = c.get('/menu/')
		self.assertEqual(response.status_code, 200)

	def test_open_canteen_form_page(self):
		c = Client()
		response = c.get('/menu/add_canteen_form/')
		self.assertEqual(response.status_code, 200)

	def test_add_canteen_redirect(self):
		c = Client()
		response = c.get('/menu/add_canteen/')
		self.assertEqual(response.status_code, 302)

	def test_add_canteen_post(self):
		c = Client()
		to_be_posted = {
			'csrfmiddlewaretoken': 'CSRF Token',
			'nama': 'Nama Kantin',
			'jam_buka': '00:00',
			'jam_tutup': '00:00',
			'kategori': 'Kategori Kantin',
			'gambar': 'https://'
		}
		c.post('/menu/add_canteen/', data=to_be_posted)
		response = c.get('/menu/')
		content = response.content.decode('utf8')
		self.assertIn(to_be_posted['nama'], content)
		self.assertIn('midnight', content)
		self.assertIn('midnight', content)
		self.assertIn(to_be_posted['kategori'], content)
		self.assertIn(to_be_posted['gambar'], content)

	def test_model_canteen_and_fields(self):
		Canteen(
			nama= "Nama Kantin",
			jam_buka= "00:00",
			jam_tutup= "00:00",
			kategori= "Kategori Kantin",
			gambar= "Gambar Kantin").save();

		self.assertEqual(Canteen.objects.all().count(), 1)

class MenuTest(TestCase):
	def test_apakah_page_menu_bisa_dibuka(self):
		a = Client()
		response = a.get('/menu/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_page_form_menu_bisa_dibuka(self):
		a = Client()
		response = a.get('/menu/add_menu_form/')
		self.assertEqual(response.status_code, 200)

	# def test_apakah_bisa_menambahkan_menu(self):
	# 	a = Client()
	# 	response = a.get('/menu/add_menu/')
	# 	self.assertEqual(response.status_code, 302)

	# def test_apakah_bisa_post_tambah_menu(self):
	# 	a = Client()
	# 	to_be_posted = {
	# 		'csrfmiddlewaretoken': 'CSRF Token',
	# 		'Nama': 'Nama Makanan/Minuman',
	# 		'Harga':'Rp.',
	# 		'Kategori': 'Kategori Menu',
	# 		'Gambar': 'https://',
	# 		'Deskripsi': 'Deskripsi Menu',
	# 	}
	# 	a.post('/menu/add_menu/', data=to_be_posted)
	# 	response = a.get('/menu/')
	# 	content = response.content.decode('utf8')
	# 	self.assertIn(to_be_posted['Nama'], content)
	# 	self.assertIn(to_be_posted['Harga'], content)
	# 	self.assertIn(to_be_posted['Kategori'], content)
	# 	self.assertIn(to_be_posted['Gambar'], content)
	# 	self.assertIn(to_be_posted['Deskripsi'], content)

	# def test_apakah_ada_model_menu_ada_di_fields(self):
	# 	Menu(
	# 		Nama= "Nama Menu",
	# 		Harga= "Rp.",
	# 		Kategori= "Kategori Menu",
	# 		Gambar= "Gambar Menu",
	# 		Deskripsi= "Deskripsi Menu").save();

	# 	self.assertEqual(Menu.objects.all().count(), 1)

	# def test_apakah_ada_button_tambah_menu(self):
	# 	a = Client()
	# 	response = a.get('/menu/')
	# 	content = response.content.decode('utf8')
	# 	self.assertIn("<button", content)	
	# 	self.assertIn("Tambah Menu", content)




